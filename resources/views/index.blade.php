<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
</head>
<body style="padding-top: 100px; padding-left: 200px">


    <button type="button" id="btn" class="btn btn-success">Success</button>

    <script>
        $(document).ready(function(){
            $("#btn").click(function(){
                console.log("oke");
                const OneSignal = require('onesignal-node');
                // With default options
                const client = new OneSignal.Client('61eff55a-1f67-416e-94c2-211a90fbb027', 'ZDQzYmE5ZGQtY2VkOS00ZTY3LTk2NzItN2U4NTZmNmQ2ODY4');
                // See all fields: https://documentation.onesignal.com/reference/create-notification
                const notification = {
                    contents: {
                        'tr': 'Yeni bildirim',
                        'en': 'New notification',
                    },
                    included_segments: ['Subscribed Users'],
                    filters: [
                        { field: 'tag', key: 'level', relation: '>', value: 10 }
                    ]
                };

                // using async/await
                try {
                    const response = client.createNotification(notification)
                    console.log(response.body.id);
                } catch (e) {
                    if (e instanceof OneSignal.HTTPError) {
                        // When status code of HTTP response is not 2xx, HTTPError is thrown.
                        console.log(e.statusCode);
                        console.log(e.body);
                    }
                }

                // or you can use promise style:
                client.createNotification(notification)
                .then(response => {
                    console.log(response);
                })
                .catch(e => {
                    console.log(e);
                });
            });
        });
    </script>

</body>
</html>
