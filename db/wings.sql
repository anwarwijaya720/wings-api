-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 06, 2022 at 07:32 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.0.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wings`
--

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `no` int(11) NOT NULL,
  `prodict_id` varchar(225) NOT NULL,
  `product_code` varchar(100) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `price` int(10) NOT NULL,
  `currency` varchar(3) NOT NULL,
  `discount` int(3) NOT NULL,
  `dimension` int(100) NOT NULL,
  `unit` varchar(10) NOT NULL,
  `qty` int(10) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_by` varchar(50) NOT NULL,
  `updated_date` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`no`, `prodict_id`, `product_code`, `product_name`, `price`, `currency`, `discount`, `dimension`, `unit`, `qty`, `created_date`, `created_by`, `updated_date`, `updated_by`) VALUES
(1, 'example12356', 'Jr0010', 'Downi', 14000, 'IDR', 0, 50, '', 1, '2022-10-06 03:00:46', 'admin', NULL, NULL),
(2, 'example89897', 'Jr0020', 'Mie Sedap', 3000, 'IDR', 0, 10, '', 100, '2022-10-06 03:00:35', 'admin', NULL, NULL),
(3, 'example1234132413', 'Jr0030', 'Ekonomi', 7000, 'IDR', 0, 30, '', 190, '2022-10-06 03:00:52', 'admin', NULL, NULL),
(4, 'example890909', 'Jr1040', 'So Klin', 14000, 'IDR', 0, 50, '', 10, '2022-10-06 03:00:56', 'admin', NULL, NULL),
(5, 'example5656590', 'Jr0050', 'Daia', 14000, 'IDR', 0, 50, '', 17, '2022-10-06 03:01:02', 'admin', NULL, NULL),
(6, 'example12315216', 'Jr0060', 'Kecap Sedap', 14000, 'IDR', 0, 50, '', 8, '2022-10-06 03:01:09', 'admin', NULL, NULL),
(7, 'example526712581', 'Jr0080', 'Ciptaden', 14000, 'IDR', 0, 50, '', 50, '2022-10-06 03:01:14', 'admin', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `trnsaction`
--

CREATE TABLE `trnsaction` (
  `no` int(11) NOT NULL,
  `transaction_id` varchar(100) NOT NULL,
  `document_code` varchar(100) NOT NULL,
  `document_number` varchar(100) NOT NULL,
  `user` varchar(100) NOT NULL,
  `total` int(10) NOT NULL,
  `transaction_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `no` int(11) NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`no`, `user_id`, `username`, `password`, `create_date`, `status`) VALUES
(1, 'sghagshags1231', 'admin', '21232f297a57a5a743894a0e4a801fc3', '2022-10-06 02:30:24', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `trnsaction`
--
ALTER TABLE `trnsaction`
  ADD PRIMARY KEY (`no`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`no`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `trnsaction`
--
ALTER TABLE `trnsaction`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `no` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
