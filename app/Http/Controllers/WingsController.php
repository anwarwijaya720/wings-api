<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Ramsey\Uuid\Uuid;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;

class WingsController extends AuthController {
    
    function users(Request $data){
        $query = DB::select("SELECT * FROM user");

        if(count($query) > 0){
            $results = [
                "code" => 200,
                "info" => "sukses",
                "data" => $query
            ];
        }else{
            $results = [
                "code" => 500,
                "info" => "data tidak di temukan",
            ];
        }

        return response()->json($results);
    }
    
    function login(Request $data){
        $nis = $data->input('name');
        $pin = md5($data->input('pass'));
        // $fcm = $data->input('fcm_token');

        $where = array(
            'username' => $nis,
            'password' => $pin,
        );

        $query = DB::table('user')
                ->select('*')
                ->where($where)->get();

        if(count($query) > 0){
            $payload = [
                'iss' => "lumen-jwt", // Issuer of the token
                'userId' => $query[0]->user_id, // Subject of the token
                // 'iat' => time(), // Time when JWT was issued.
                // 'exp' => time() + 60*60 // Expiration time
            ];

            $token =  JWT::encode($payload, env('JWT_SECRET'), 'HS256');
            $status = $query[0]->status;

            if($status == 1){
                $results = [
                    "code" => 200,
                    "info" =>  "login sukses",
                    "token" => $token,
                    "data" => $query
                ];
            }else{
                $results = [
                    "code" => 404,
                    "info" => "User Not Fount",
                ];
            }
        }else{
            $results = [
                "code" => 500,
                "info" => "Terjasi Kesalahan",
            ];
        }

        return response()->json($results);
    }

    function product(Request $data){
        //cek token
        $res = parent::cekToken($data);
        if($res["code"] != 200){
            return $res;
        }

        $query = DB::select("SELECT * FROM product");

        if(count($query) > 0){
            $results = [
                "code" => 200,
                "info" => "sukses",
                "data" => $query
            ];
        }else{
            $results = [
                "code" => 404,
                "info" => "Product Not Fount",
            ];
        }

        return response()->json($results);

    }

    
}