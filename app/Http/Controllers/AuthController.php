<?php

namespace App\Http\Controllers;

use App\Models\User;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;


class AuthController
{
    function cekToken(Request $data){
        $auth = $data->header('Authorization');
        if($auth == ''){
            $results = [
                "code" => 401,
                "info" => "Token not provided.",
            ];
            return $results;
        }
        $auth = explode(' ', $auth);
        if($auth[0] != 'Bearer'){
            $results = [
                "code" => 401,
                "info" => "format error occurred.",
            ];
            return $results;
        }
        $token = $auth[1];
        try {
            $decode = JWT::decode($token, new Key(env('JWT_SECRET'), 'HS256'));
            $query = DB::select("SELECT * FROM user Where user_id = '$decode->userId' ");
            if(count($query) > 0){
                $results = [
                    "code" => 200,
                    "info" => "sukses"
                ];
                return $results;
            }else{
                $results = [
                    "code" => 500,
                    "info" => "token not available",
                ];
                return $results;
            }
        } catch(ExpiredException $e) {
            $results = [
                "code" => 405,
                "info" => "Provided token is expired.",
            ];
            return $results;
        } catch (\Throwable $th) {
            $results = [
                "code" => 400,
                "info" => "there is an error",
            ];
            return $results;
        }
    }

}
