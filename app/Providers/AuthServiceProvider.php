<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.

        $this->app['auth']->viaRequest('api', function ($request) {
            // if ($request->input('api_token')) {
            //     return User::where('api_token', $request->input('api_token'))->first();
            // }
            
            $auth = $request->header('Authorization');
            if($auth == ''){
                return response()->json([                    
                    "code" => 401,
                    "info" => "Token not provided.",
                ]);
            }
            $auth = explode(' ', $auth);
            if($auth[0] != 'Bearer'){
                return response()->json([
                    "code" => 402,
                    "info" => "format error occurred.",
                ]);
            }
            $token = $auth[1];
            try {
                $decode = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
                return User::find($decode->key);
            } catch(ExpiredException $e) {
                return response()->json([
                    "code" => 400,
                    "info" => "Provided token is expired.",
                ]);
            } catch (\Throwable $th) {
                return response()->json([
                    "code" => 400,
                    "info" => $th,
                ]);
            }
        });
    }
}